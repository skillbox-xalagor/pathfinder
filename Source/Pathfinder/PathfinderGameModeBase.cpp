// Copyright Epic Games, Inc. All Rights Reserved.


#include "PathfinderGameModeBase.h"

#include "MazeSelector.h"
#include "PathfinderCharacter.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "Maze/FinishTriggerBox.h"

APathfinderGameModeBase::APathfinderGameModeBase()
{
	DefaultPawnClass = APathfinderCharacter::StaticClass();
}

void APathfinderGameModeBase::CleanUpMazes() {
	for (const auto Element : Mazes)
	{
		if (IsValid(Element))
		{
			Element->Destroy();
		}
	}
	Mazes.Empty();

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AMaze::StaticClass(), FoundActors);
	for (auto FoundActor : FoundActors)
	{
		if (IsValid(FoundActor))
		{
			FoundActor->Destroy();
		}
	}
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AFinishTriggerBox::StaticClass(), FoundActors);
	for (auto FoundActor : FoundActors)
	{
		if (IsValid(FoundActor))
		{
			FoundActor->Destroy();
		}
	}
}

void APathfinderGameModeBase::RestartPathfinder() {
	APlayerController* MyPc = GetWorld()->GetFirstPlayerController();
	auto               MyPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	if (MyPawn)
	{
		MyPawn->SetActorLocation(StartingPoint + FVector(0, 0, 200), false, nullptr, ETeleportType::TeleportPhysics);
		UStaticMeshComponent* MyStaticMeshComponent = Cast<UStaticMeshComponent>(MyPawn->GetRootComponent());
		if (MyStaticMeshComponent)
		{
			MyStaticMeshComponent->SetSimulatePhysics(true);
		}
	}
	if (InWidgetToFocus)
	{
		UWidgetBlueprintLibrary::SetInputMode_GameAndUIEx(MyPc, InWidgetToFocus, EMouseLockMode::LockOnCapture);
	}
}

void APathfinderGameModeBase::GenerateMazes(const int32 Seed, const FMazeSize MazeSize, const bool bGeneratePath, const int32 LevelsCount)
{
	CleanUpMazes();

	for (int i = 0; i < LevelsCount; ++i)
	{
		const FActorSpawnParameters SpawnInfo;
		AMaze*                      NewMaze = GetWorld()->SpawnActor<AMaze>(StartingPoint, FRotator(0.0f), SpawnInfo);
		if  (NewMaze)
		{
		    NewMaze->Seed = Seed + i;
            NewMaze->MazeSize.X = MazeSize.X;
			NewMaze->MazeSize.Y = MazeSize.Y;
            NewMaze->FloorStaticMesh = FloorStaticMesh;
            NewMaze->WallStaticMesh = WallStaticMesh;
            NewMaze->OutlineStaticMesh = OutlineStaticMesh;
            NewMaze->PathStart = PathStart;
            NewMaze->PathEnd.X = MazeSize.X - 1;
			NewMaze->PathEnd.Y = MazeSize.Y - 1;
            NewMaze->PathStaticMesh = PathStaticMesh;
            NewMaze->Finish = FMazeCoordinates(MazeSize.X, MazeSize.Y);
            NewMaze->bGeneratePath = bGeneratePath;
            NewMaze->UpdateMaze();
            NewMaze->SetActorRelativeLocation(FVector(0, 0, StartingPoint.Z - (NewMaze->GetMaxCellSize().Y + FloorDelta) * i));
            Mazes.Add(NewMaze);
		}
	}
	RestartPathfinder();
}

void APathfinderGameModeBase::GenerateRandom(const bool bGeneratePath, const int32 LevelsCount)
{
	CleanUpMazes();
	for (int i = 0; i < LevelsCount; ++i)
	{
		const FActorSpawnParameters SpawnInfo;
		AMaze*                      NewMaze = GetWorld()->SpawnActor<AMaze>(StartingPoint, FRotator(0.0f), SpawnInfo);
		if  (NewMaze)
		{
			NewMaze->FloorStaticMesh = FloorStaticMesh;
			NewMaze->WallStaticMesh = WallStaticMesh;
			NewMaze->OutlineStaticMesh = OutlineStaticMesh;
			NewMaze->PathStart = PathStart;
			NewMaze->PathStaticMesh = PathStaticMesh;
			NewMaze->bGeneratePath = bGeneratePath;
			NewMaze->Randomize();
			NewMaze->SetActorRelativeLocation(FVector(0, 0, StartingPoint.Z - (NewMaze->GetMaxCellSize().Y + FloorDelta) * i));
			Mazes.Add(NewMaze);
		}
	}
	RestartPathfinder();
}

void APathfinderGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	const auto     GameInstance = GetGameInstance();
	InWidgetToFocus = CreateWidget<UMazeSelector>(GameInstance, MazeSelectorWidget);
	if (InWidgetToFocus)
	{
		InWidgetToFocus->AddToViewport();
		auto MyPc = GetWorld()->GetFirstPlayerController();
		UWidgetBlueprintLibrary::SetInputMode_UIOnlyEx(MyPc, InWidgetToFocus, EMouseLockMode::LockOnCapture);
		MyPc->SetShowMouseCursor(true);
		InWidgetToFocus->MyGm = this;
	}
}
