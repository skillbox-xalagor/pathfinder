﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PathfinderGameModeBase.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/CheckBox.h"
#include "Components/SpinBox.h"
#include "CommonTextBlock.h"
#include "MazeSelector.generated.h"

/**
 * 
 */
UCLASS(Abstract, Blueprintable, BlueprintType, ClassGroup = UI)
class PATHFINDER_API UMazeSelector : public UUserWidget
{
	GENERATED_BODY()

protected:
	UFUNCTION()
	void            GenerateMaze();
	UFUNCTION()
	void            GenerateRandom();
	UFUNCTION()
	void QuitGame();
	// Doing setup in the C++ constructor is not as
	// useful as using NativeConstruct.
	virtual void NativeConstruct() override;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	USpinBox* SpinBox_Seed;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	USpinBox* SpinBox_MazeSizeX;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	USpinBox* SpinBox_MazeSizeY;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UCheckBox* CheckBox_GeneratePath;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	USpinBox* SpinBox_LevelsCount;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* Button_GenerateMaze;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* Button_GenerateRandom;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UButton* Button_Quit;
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UCommonTextBlock* CommonTextBlock_GenerateMaze;

public:
	UPROPERTY(BlueprintReadWrite)
	APathfinderGameModeBase* MyGm = nullptr;
};
