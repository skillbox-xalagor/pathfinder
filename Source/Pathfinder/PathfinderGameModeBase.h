// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Maze/Maze.h"
#include "PathfinderGameModeBase.generated.h"

/**
 * GameMode of rolling game wih maze generation
 */
UCLASS()
class PATHFINDER_API APathfinderGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	APathfinderGameModeBase();
	void         CleanUpMazes();
	void         RestartPathfinder();
	virtual void BeginPlay() override;

	UPROPERTY()
	class UMazeSelector* InWidgetToFocus = nullptr;

public:
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Maze Setup",
		meta=(ClampMin=1, UIMin=1, UIMax=101, ClampMax=101, NoResetToDefault))
	int32 FloorDelta = 250;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName="Generation Starting point", Category="Maze Setup")
	FVector StartingPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName="Floor", Category="Maze|Cells",
		meta=(NoResetToDefault, DisplayPriority=0))
	UStaticMesh* FloorStaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName="Wall", Category="Maze|Cells",
		meta=(NoResetToDefault, DisplayPriority=1))
	UStaticMesh* WallStaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName="Outline Wall", Category="Maze|Cells",
		meta=(DisplayPriority=2))
	UStaticMesh* OutlineStaticMesh;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Maze|Pathfinder",
		meta=(EditCondition="bGeneratePath", EditConditionHides))
	FMazeCoordinates PathStart;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Maze|Pathfinder",
		meta=(EditCondition="bGeneratePath", EditConditionHides))
	FMazeCoordinates PathEnd;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName="Path Floor", Category="Maze|Pathfinder",
		meta=(EditCondition="bGeneratePath", EditConditionHides))
	UStaticMesh* PathStaticMesh;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Maze")
	FMazeCoordinates Finish;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="UI")
	TSubclassOf<UUserWidget> MazeSelectorWidget;

	TArray<AMaze*> Mazes;

	UFUNCTION(BlueprintCallable)
	void GenerateMazes(int32 Seed, FMazeSize MazeSize, bool bGeneratePath, int32 LevelsCount);
	void GenerateRandom(const bool bGeneratePath, const int32 LevelsCount);
};
