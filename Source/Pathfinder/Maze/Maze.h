#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Eller.h"
#include "Components/BoxComponent.h"
#include "Maze.generated.h"

class AFinishTriggerBox;
DECLARE_LOG_CATEGORY_EXTERN(LogMaze, Warning, All);

struct FIntVector2;

USTRUCT(BlueprintType)
struct FMazeSize
{
	GENERATED_BODY()

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Maze",
		meta=(ClampMin=3, UIMin=5, UIMax=101, ClampMax=9999, NoResetToDefault))
	int32 X;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Maze",
		meta=(ClampMin=3, UIMin=5, UIMax=101, ClampMax=9999, NoResetToDefault))
	int32 Y;

	FMazeSize();
	
	FMazeSize(const int32 InX, const int32 InY) : X(InX), Y(InY) {}

	operator FIntVector2() const;
};

USTRUCT(BlueprintType)
struct FMazeCoordinates
{
	GENERATED_BODY()

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Maze",
		meta=(NoSpinbox=true, ClampMin=0, NoResetToDefault))
	int32 X;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Maze",
		meta=(NoSpinbox=true, ClampMin=0, Delta=1, NoResetToDefault))
	int32 Y;

	FMazeCoordinates();

	FMazeCoordinates(const int32 InX, const int32 InY) : X(InX), Y(InY) {}

	void ClampByMazeSize(const FMazeSize& MazeSize);

	bool operator==(const FMazeCoordinates& Other) const;

	bool operator!=(const FMazeCoordinates& Other) const;

	operator TPair<int32, int32>() const;
};

class Eller;
class UHierarchicalInstancedStaticMeshComponent;

UCLASS()
class PATHFINDER_API AMaze : public AActor
{
	GENERATED_BODY()

public:
	UFUNCTION()
	void OnMazeFinish(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	AMaze();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Maze", meta=(ExposeOnSpawn, DisplayPriority=0))
	int32 Seed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Maze", meta=(ExposeOnSpawn, DisplayPriority=1))
	FMazeSize MazeSize;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName="Floor", Category="Maze|Cells",
		meta=(NoResetToDefault, ExposeOnSpawn, DisplayPriority=0))
	UStaticMesh* FloorStaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName="Wall", Category="Maze|Cells",
		meta=(NoResetToDefault, ExposeOnSpawn, DisplayPriority=1))
	UStaticMesh* WallStaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName="Outline Wall", Category="Maze|Cells",
		meta=(ExposeOnSpawn, DisplayPriority=2))
	UStaticMesh* OutlineStaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Maze|Pathfinder", meta=(ExposeOnSpawn))
	bool bGeneratePath = false;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Maze|Pathfinder",
		meta=(ExposeOnSpawn, EditCondition="bGeneratePath", EditConditionHides))
	FMazeCoordinates PathStart;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Maze|Pathfinder",
		meta=(ExposeOnSpawn, EditCondition="bGeneratePath", EditConditionHides))
	FMazeCoordinates PathEnd;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, DisplayName="Path Floor", Category="Maze|Pathfinder",
		meta=(ExposeOnSpawn, EditCondition="bGeneratePath", EditConditionHides))
	UStaticMesh* PathStaticMesh;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category="Maze|Pathfinder",
		meta=(EditCondition="bGeneratePath", EditConditionHides))
	int32 PathLength;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Maze")
	bool bUseCollision = true;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Maze",
		meta=(ExposeOnSpawn))
	FMazeCoordinates Finish;

	UPROPERTY()
	UBoxComponent* FinishTrigger;

protected:
	TArray<TArray<uint8>> MazeGrid;

	TArray<TArray<uint8>> MazePathGrid;

	UPROPERTY()
	UHierarchicalInstancedStaticMeshComponent* FloorCells;

	UPROPERTY()
	UHierarchicalInstancedStaticMeshComponent* WallCells;

	UPROPERTY()
	UHierarchicalInstancedStaticMeshComponent* OutlineWallCells;

	UPROPERTY()
	UHierarchicalInstancedStaticMeshComponent* PathFloorCells;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category="Maze|Cells")
	FVector2D MazeCellSize;

public:
	UFUNCTION(BlueprintCallable, Category="Maze")
	virtual void UpdateMaze();

	virtual void OnConstruction(const FTransform& Transform) override;

	virtual TArray<TArray<uint8>> GetMazePath(const FMazeCoordinates& Start, const FMazeCoordinates& End,
		int32&                                                        OutLength);

	virtual FVector2D GetMaxCellSize() const;

	UFUNCTION(CallInEditor, Category="Maze", meta=(DisplayPriority=0, ShortTooltip = "Generate an arbitrary maze."))
	virtual void Randomize();

protected:

	virtual void CreateMazeOutline() const;

	virtual void EnableCollision(const bool bShouldEnable);

	// Clears all HISM instances.
	virtual void ClearMaze() const;

	#if WITH_EDITOR

private:
	FTransform LastMazeTransform;
	#endif
};
