﻿#include "Eller.h"

EDirection OppositeDirection(const EDirection Direction)
{
	switch (Direction)
	{
		case EDirection::East:
			return EDirection::West;
		case EDirection::West:
			return EDirection::East;
		case EDirection::South:
			return EDirection::North;
		case EDirection::North:
			return EDirection::South;
		default:
			return EDirection::None;
	}
}

int32 DirectionDX(const EDirection Direction)
{
	switch (Direction)
	{
		case EDirection::East:
			return 1;
		case EDirection::West:
			return -1;
		default:
			return 0;
	}
}

int32 DirectionDY(const EDirection Direction)
{
	switch (Direction)
	{
		case EDirection::North:
			return -1;
		case EDirection::South:
			return 1;
		default:
			return 0;
	}
}

TArray<TArray<uint8>> Eller::GetGrid(const FIntVector2& Size, const int32 Seed)
{
	// There is for each 2 not connected floors 1 wall between.
	const FIntVector2 DirectionsGridSize((Size.X + 1) / 2, (Size.Y + 1) / 2);

	const FRandomStream RandomStream(Seed);

	const TArray<TArray<uint8>> DirectionsGrid = GetDirectionsGrid(DirectionsGridSize, RandomStream);

	TArray<TArray<uint8>> Grid = CreateZeroedGrid(Size);

	for (int32 Y = 0; Y < DirectionsGridSize.Y; ++Y)
	{
		for (int32 X = 0; X < DirectionsGridSize.X; ++X)
		{
			Grid[Y * 2][X * 2] = 1;

			//It only makes sense to check the western and northern directions,
			//because the remaining ones would simply overlap.

			if (DirectionsGrid[Y][X] & static_cast<uint8>(EDirection::West))
			{
				Grid[Y * 2][X * 2 - 1] = 1;
			}
			if (DirectionsGrid[Y][X] & static_cast<uint8>(EDirection::North))
			{
				Grid[Y * 2 - 1][X * 2] = 1;
			}
		}
	}

	return Grid;
}

TArray<TArray<uint8>> Eller::CreateZeroedGrid(const FIntVector2& Size)
{
	TArray<TArray<uint8>> Grid;
	Grid.Init(TArray<uint8>(), Size.Y);
	for (int32 Y = 0; Y < Size.Y; ++Y)
	{
		Grid[Y].SetNumZeroed(Size.X);
	}
	return Grid;
}


TArray<TArray<uint8>> Eller::GetDirectionsGrid(const FIntVector2& Size, const FRandomStream& RandomStream)
{
	TArray<TArray<uint8>> Grid = CreateZeroedGrid(Size);

	TArray<uint32> Row;
	Row.SetNumZeroed(Size.X);

	uint32 SetsCounter = 0;

	// Initialize all rows except last one.
	for (int32 Y = 0; Y < Size.Y; ++Y)
	{
		for (int32 X = 0; X < Size.X; ++X)
		{
			if (!Row[X])
			{
				Row[X] = ++SetsCounter;
			}
		}

		if (Y == Size.Y - 1)
		{
			break;
		}

		for (int32 X = 0; X < Size.X - 1; ++X)
		{
			if (Row[X] != Row[X + 1] && !RandomStream.RandRange(0, 1))
			{
				Grid[Y][X] |= static_cast<uint8>(EDirection::East);
				Grid[Y][X + 1] |= static_cast<uint8>(EDirection::West);

				const uint32 DissolvedSet = Row[X + 1];
				do
				{
					Row[X + 1] = Row[X];
					X++;
				}
				while (X < Size.X - 1 && Row[X + 1] == DissolvedSet);
			}
		}

		// Create vertical passages.
		for (int32 PassagesCount = 0, CurrentSet,
		           CellsAmount = 1, // For current set.
		           X = 0; X < Size.X; ++X, ++CellsAmount)
		{
			CurrentSet = Row[X];
			if (RandomStream.RandRange(0, 1))
			{
				++PassagesCount;

				Grid[Y][X] |= static_cast<uint8>(EDirection::South);
				Grid[Y + 1][X] |= static_cast<uint8>(EDirection::North);
			}
			else
			{
				Row[X] = 0;
			}

			if (X == Size.X - 1              // If last cell. 
				|| CurrentSet != Row[X + 1]) // If set is about to change.
			{
				//Ensure there at least one vertical passage in previous set.
				if (!PassagesCount)
				{
					const int32 RandomX = RandomStream.RandRange(X - CellsAmount + 1, X);

					Row[RandomX] = CurrentSet;

					Grid[Y][RandomX] |= static_cast<uint8>(EDirection::South);
					Grid[Y + 1][RandomX] |= static_cast<uint8>(EDirection::North);
				}

				PassagesCount = 0;
				CellsAmount = 0;
			}
		}
	}

	// Create last row.
	for (int X = 0; X < Size.X - 1; ++X)
	{
		if (Row[X] != Row[X + 1])
		{
			Grid[Size.Y - 1][X] |= static_cast<uint8>(EDirection::East);
			Grid[Size.Y - 1][X + 1] |= static_cast<uint8>(EDirection::West);

			const uint32 DissolvedSet = Row[X + 1];
			do
			{
				Row[X + 1] = Row[X];
				X++;
			}
			while (X < Size.X - 1 && Row[X + 1] == DissolvedSet);

			// After this loop, X is the index of the last merged element,
			// and then the outer loop will increment X, so it is needed to decrement once 
			--X;
		}
	}

	return Grid;
}
