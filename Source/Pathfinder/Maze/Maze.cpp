#include "Maze.h"

#include "FinishTriggerBox.h"
#include "Async/Async.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Framework/Notifications/NotificationManager.h"
#include "Pathfinder/PathfinderCharacter.h"
#include "Widgets/Notifications/SNotificationList.h"

DEFINE_LOG_CATEGORY(LogMaze);

FMazeSize::FMazeSize()
	: X(5)
	, Y(5) {}

FMazeSize::operator FIntVector2() const
{
	return FIntVector2{ X, Y };
}

FMazeCoordinates::FMazeCoordinates()
	: X(0)
	, Y(0) {}

void FMazeCoordinates::ClampByMazeSize(const FMazeSize& MazeSize)
{
	if (X >= MazeSize.X)
	{
		X = MazeSize.X - 1;
	}
	if (Y >= MazeSize.Y)
	{
		Y = MazeSize.Y - 1;
	}
}

bool FMazeCoordinates::operator==(const FMazeCoordinates& Other) const
{
	return X == Other.X && Y == Other.Y;
}

bool FMazeCoordinates::operator!=(const FMazeCoordinates& Other) const
{
	return !(*this == Other);
}

FMazeCoordinates::operator TTuple<int, int>() const
{
	return TPair<int32, int32>{ X, Y };
}

void AMaze::OnMazeFinish(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->IsA(APathfinderCharacter::StaticClass()))
	{
		OtherActor->SetActorLocation(GetActorLocation() - FVector(0, 0, 200), false, nullptr, ETeleportType::TeleportPhysics);
		Destroy();
	}
}

AMaze::AMaze()
{
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	FloorCells = CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>(TEXT("FloorCells"));
	if (FloorCells)
	{
		FloorCells->SetupAttachment(GetRootComponent());
	}

	PathFloorCells = CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>(TEXT("PathFloorCells"));
	if (PathFloorCells)
	{
		PathFloorCells->SetupAttachment(GetRootComponent());
	}

	WallCells = CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>(TEXT("WallCells"));
	if (WallCells)
	{
		WallCells->SetupAttachment(GetRootComponent());
	}

	OutlineWallCells = CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>(TEXT("OutlineWallCells"));
	if (OutlineWallCells)
	{
		OutlineWallCells->SetupAttachment(GetRootComponent());
	}

	FinishTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("FinishTrigger"));
	if (FinishTrigger)
	{
		FinishTrigger->SetupAttachment(GetRootComponent());
	}
	FinishTrigger->OnComponentBeginOverlap.AddDynamic(this, &AMaze::OnMazeFinish);
	FinishTrigger->SetHiddenInGame(false);
}

void AMaze::UpdateMaze()
{
	ClearMaze();

	if (!(FloorStaticMesh && WallStaticMesh))
	{
		UE_LOG(LogMaze, Warning, TEXT("To create maze specify FloorStaticMesh and WallStaticMesh"));
		#if WITH_EDITOR
		#define LOCTEXT_NAMESPACE "Pathfinder"
		FNotificationInfo Info(LOCTEXT("SpecifyMeshes", "To create maze specify FloorStaticMesh and WallStaticMesh"));
		const auto        NotificationItem = FSlateNotificationManager::Get().AddNotification(Info);
		#undef LOCTEXT_NAMESPACE
		#endif
	}

	FloorCells->SetStaticMesh(FloorStaticMesh);
	WallCells->SetStaticMesh(WallStaticMesh);
	if (OutlineStaticMesh)
	{
		OutlineWallCells->SetStaticMesh(OutlineStaticMesh);
	}
	if (PathStaticMesh)
	{
		PathFloorCells->SetStaticMesh(PathStaticMesh);
	}

	MazeCellSize = GetMaxCellSize();

	if (OutlineStaticMesh)
	{
		CreateMazeOutline();
	}
	MazeGrid = Eller::GetGrid(MazeSize, Seed);

	if (bGeneratePath)
	{
		PathStart.ClampByMazeSize(MazeSize);
		PathEnd.ClampByMazeSize(MazeSize);
		MazePathGrid = GetMazePath(PathStart, PathEnd, PathLength);
	}

	for (int32 Y = 0; Y < MazeSize.Y; ++Y)
	{
		for (int32 X = 0; X < MazeSize.X; ++X)
		{
			if (Y == Finish.Y - 1 && X == Finish.X - 1)
			{
				const auto    MyLocation = GetActorLocation();
				const FVector Location(MyLocation.X + MazeCellSize.X * X, MyLocation.Y + MazeCellSize.Y * Y, MyLocation.Z);
				if (IsValid(FinishTrigger))
				{
					FinishTrigger->SetRelativeLocation(Location);
				}
			}
			if (bGeneratePath && PathStaticMesh && MazePathGrid.Num() > 0 && MazePathGrid[Y][X])
			{
				const FVector Location(MazeCellSize.X * X, MazeCellSize.Y * Y, 0.f);
				PathFloorCells->AddInstance(FTransform(Location));
			}
			else if (MazeGrid[Y][X])
			{
				const FVector Location{ MazeCellSize.X * X, MazeCellSize.Y * Y, 0.f };
				FloorCells->AddInstance(FTransform(Location));
			}
			else
			{
				const FVector Location{ MazeCellSize.X * X, MazeCellSize.Y * Y, 0.f };
				WallCells->AddInstance(FTransform(Location));
			}
		}
	}

	EnableCollision(bUseCollision);
}

void AMaze::CreateMazeOutline() const
{
	FVector Location1{ 0.f };
	FVector Location2{ 0.f };

	Location1.Y = -MazeCellSize.Y;
	Location2.Y = MazeCellSize.Y * MazeSize.Y;
	for (int32 X = -1; X < MazeSize.X + 1; ++X)
	{
		Location1.X = Location2.X = X * MazeCellSize.X;
		OutlineWallCells->AddInstance(FTransform{ Location1 });
		OutlineWallCells->AddInstance(FTransform{ Location2 });
	}

	Location1.X = -MazeCellSize.X;
	Location2.X = MazeCellSize.X * MazeSize.X;
	for (int32 Y = 0; Y < MazeSize.Y; ++Y)
	{
		Location1.Y = Location2.Y = Y * MazeCellSize.Y;
		OutlineWallCells->AddInstance(FTransform{ Location1 });
		OutlineWallCells->AddInstance(FTransform{ Location2 });
	}
}

TArray<TArray<uint8>> AMaze::GetMazePath(const FMazeCoordinates& Start, const FMazeCoordinates& End, int32& OutLength)
{
	TArray<TArray<int32>> Graph;
	Graph.Reserve(MazeGrid.Num() * MazeGrid[0].Num());

	// Graph creation.
	for (int32 GraphVertex,
	           Y = 0; Y < MazeGrid.Num(); ++Y)
	{
		for (int32 X = 0; X < MazeGrid[Y].Num(); ++X)
		{
			GraphVertex = Y * MazeGrid[Y].Num() + X;

			Graph.Emplace(TArray<int32>());
			if (!MazeGrid[Y][X])
			{
				continue;
			}

			Graph[GraphVertex].Reserve(4); // There are only 4 directions possible.

			if (X > 0 && MazeGrid[Y][X - 1]) // West direction.
			{
				Graph[GraphVertex].Emplace(GraphVertex - 1);
			}
			if (X + 1 < MazeGrid[Y].Num() && MazeGrid[Y][X + 1]) // East direction.
			{
				Graph[GraphVertex].Emplace(GraphVertex + 1);
			}
			if (Y > 0 && MazeGrid[Y - 1][X]) // North direction.
			{
				Graph[GraphVertex].Emplace(GraphVertex - MazeGrid[Y].Num());
			}
			if (Y + 1 < MazeGrid.Num() && MazeGrid[Y + 1][X]) // South direction.
			{
				Graph[GraphVertex].Emplace(GraphVertex + MazeGrid[Y].Num());
			}

			Graph[GraphVertex].Shrink();
		}
	}

	const int32 StartVertex = Start.Y * MazeGrid[0].Num() + Start.X;
	const int32 EndVertex = End.Y * MazeGrid[0].Num() + End.X;

	TQueue<int32> Vertices;

	const int32 VerticesAmount = MazeGrid.Num() * MazeGrid[0].Num();

	TArray<bool> Visited;
	Visited.Init(false, VerticesAmount);

	TArray<int32> Parents;
	Parents.Init(-1, VerticesAmount);

	TArray<int32> Distances;
	Distances.Init(0, VerticesAmount);

	int32 Vertex;
	Vertices.Enqueue(StartVertex);
	Visited[StartVertex] = true;
	while (Vertices.Dequeue(Vertex))
	{
		for (int32 i = 0; i < Graph[Vertex].Num(); ++i)
		{
			const int32 Adjacent = Graph[Vertex][i];
			if (!Visited[Adjacent])
			{
				Visited[Adjacent] = true;
				Vertices.Enqueue(Adjacent);
				Distances[Adjacent] = Distances[Vertex] + 1;
				Parents[Adjacent] = Vertex;
			}
		}
	}

	TArray<int32> GraphPath;
	if (!Visited[EndVertex])
	{
		UE_LOG(LogMaze, Warning, TEXT("Path is not reachable"));
		#if WITH_EDITOR
		#define LOCTEXT_NAMESPACE "Pathfinder"
		FNotificationInfo Info(LOCTEXT("NotReachablePath", "Path is not reachable"));
		const auto        NotificationItem = FSlateNotificationManager::Get().AddNotification(Info);
		#undef LOCTEXT_NAMESPACE
		#endif
		return TArray<TArray<uint8>>();
	}

	for (int VertexNumber = EndVertex; VertexNumber != -1; VertexNumber = Parents[VertexNumber])
	{
		GraphPath.Emplace(VertexNumber);
	}

	Algo::Reverse(GraphPath);

	TArray<TArray<uint8>> Path;
	Path.Init(TArray<uint8>(), MazeGrid.Num());
	for (int Y = 0; Y < MazeGrid.Num(); ++Y)
	{
		Path[Y].SetNumZeroed(MazeGrid[Y].Num());
	}

	for (int32 VertexNumber, i = 0; i < GraphPath.Num(); ++i)
	{
		VertexNumber = GraphPath[i];

		Path[VertexNumber / MazeGrid[0].Num()][VertexNumber % MazeGrid[0].Num()] = 1;
	}

	OutLength = Distances[EndVertex] + 1;
	return Path;
}

void AMaze::EnableCollision(const bool bShouldEnable)
{
	if (bShouldEnable)
	{
		FloorCells->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		WallCells->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		OutlineWallCells->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		PathFloorCells->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	}
	else
	{
		FloorCells->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		WallCells->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		OutlineWallCells->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		PathFloorCells->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}


void AMaze::ClearMaze() const
{
	FloorCells->ClearInstances();
	WallCells->ClearInstances();
	OutlineWallCells->ClearInstances();
	PathFloorCells->ClearInstances();
}

FVector2D AMaze::GetMaxCellSize() const
{
	const FVector FloorSize3D = FloorStaticMesh->GetBoundingBox().GetSize();
	const FVector WallSize3D = WallStaticMesh->GetBoundingBox().GetSize();

	const FVector2D FloorSize2D{ FloorSize3D.X, FloorSize3D.Y };
	const FVector2D WallSize2D{ WallSize3D.X, WallSize3D.Y };

	const FVector2D MaxCellSize = FVector2D::Max(FloorSize2D, WallSize2D);
	if (OutlineStaticMesh)
	{
		const FVector   OutlineSize3D = OutlineStaticMesh->GetBoundingBox().GetSize();
		const FVector2D OutlineSize2D{ OutlineSize3D.X, OutlineSize3D.Y };
		if (OutlineSize2D.X > MaxCellSize.X && OutlineSize2D.Y > MaxCellSize.Y)
		{
			return OutlineSize2D;
		}
	}
	return MaxCellSize;
}

void AMaze::Randomize()
{
	MazeSize.Y = FMath::RandRange(3, 101) | 1;
	MazeSize.X = FMath::RandRange(3, 101) | 1; // | 1 to make odd.

	Seed = FMath::RandRange(MIN_int32, MAX_int32);

	PathStart.X = 0;
	PathStart.Y = 0;
	PathEnd.X = MazeSize.X - 1;
	PathEnd.Y = MazeSize.Y - 1;

	UpdateMaze();

	const auto    MyLocation = GetActorLocation();
	const FVector Location(MyLocation.X + MazeCellSize.X * PathEnd.X, MyLocation.Y + MazeCellSize.Y * PathEnd.Y, MyLocation.Z);
	if (IsValid(FinishTrigger))
	{
		FinishTrigger->SetRelativeLocation(Location);
	}
}


void AMaze::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
}
