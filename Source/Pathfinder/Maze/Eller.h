﻿#pragma once

#include "CoreMinimal.h"
#include "Maze.h"

#pragma once

#include "CoreMinimal.h"

#include "Math/RandomStream.h"


struct FIntVector2
{
	int32 X{};
	int32 Y{};

	FIntVector2() = default;

	FIntVector2(int32 X, int32 Y)
		: X{ X }
		, Y{ Y } { }
};

enum class EDirection : uint8
{
	None = 0,
	East = 1,
	North = 2,
	South = 4,
	West = 8,
};

EDirection OppositeDirection(const EDirection Direction);

int32 DirectionDX(const EDirection Direction);
int32 DirectionDY(const EDirection Direction);

class Eller
{
public:
	virtual ~Eller() = default;

	static TArray<TArray<uint8>> GetGrid(const FIntVector2& Size, const int32 Seed);

protected:
	static TArray<TArray<uint8>> CreateZeroedGrid(const FIntVector2& Size);

private:
	static TArray<TArray<uint8>> GetDirectionsGrid(const FIntVector2& Size,
		const FRandomStream&                                          RandomStream);
};
