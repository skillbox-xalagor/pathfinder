﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "FinishTriggerBox.h"
#include "Pathfinder/PathfinderCharacter.h"


// Sets default values
AFinishTriggerBox::AFinishTriggerBox()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFinishTriggerBox::BeginPlay()
{
	Super::BeginPlay();
	SetActorHiddenInGame(false);
}

// Called every frame
void AFinishTriggerBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFinishTriggerBox::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	if (OtherActor->IsA(APathfinderCharacter::StaticClass()))
	{
		if (IsValid(LinkedMaze))
		{
			LinkedMaze->Destroy();
			Destroy();
		}
	}
}
