﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "MazeSelector.h"

void UMazeSelector::GenerateMaze()
{
	MyGm->GenerateMazes(
		SpinBox_Seed->GetValue(),
		FMazeSize(
			SpinBox_MazeSizeX->GetValue(),
			SpinBox_MazeSizeY->GetValue()),
		CheckBox_GeneratePath->IsChecked(),
		SpinBox_LevelsCount->GetValue()
		);
	#define LOCTEXT_NAMESPACE "Pathfinder"
	CommonTextBlock_GenerateMaze->SetText(LOCTEXT("RegenerateMaze", "Regenerate Maze"));
	#undef LOCTEXT_NAMESPACE
}

void UMazeSelector::GenerateRandom()
{
	MyGm->GenerateRandom(
		CheckBox_GeneratePath->IsChecked(),
		SpinBox_LevelsCount->GetValue()
		);
}

void UMazeSelector::QuitGame()
{
	FGenericPlatformMisc::RequestExit(false);
}

void UMazeSelector::NativeConstruct()
{
	Super::NativeConstruct();

	Button_GenerateMaze->OnClicked.AddDynamic(this, &UMazeSelector::GenerateMaze);
	Button_GenerateRandom->OnClicked.AddDynamic(this, &UMazeSelector::GenerateRandom);
	Button_Quit->OnClicked.AddDynamic(this, &UMazeSelector::QuitGame);
}
